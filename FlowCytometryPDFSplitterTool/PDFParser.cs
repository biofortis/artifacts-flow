using System;
using System.IO;
using System.Net;
using System.Net.Mime;
using System.Text;
using System.Collections.Generic;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Drawing;
using System.Drawing.Imaging;
using iTextSharp.text.pdf.parser;
using System.Linq;
using FlowCytometryPDFSplitterTool;
using Tesseract;
using RestSharp;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace PdfToText
{
    /// <summary>
    /// Parses a PDF file and extracts the text from it.
    /// </summary>
    public class PDFParser
    {
       
        #region Fields

        #region _numberOfCharsToKeep
        /// <summary>
        /// The number of characters to keep, when extracting text.
        /// </summary>
        private static int _numberOfCharsToKeep = 15;
        private int count = 0;
        #endregion

        #endregion





        public int SplitAndMergePDF(string inFileName, string outPutPath, string AssayOutputPath)
        {
            // Code
            int AICount = 0;
            string pdftext = string.Empty;
            int IndexpageCount = 0;
            try
            {
                               

                iTextSharp.text.pdf.RandomAccessFileOrArray RAFObj = null;
                iTextSharp.text.pdf.PdfReader reader = null;
                iTextSharp.text.pdf.PdfObject obj = null;
                // Create a reader for the given PDF file
                // PdfReader reader = new PdfReader(inFileName);
                //outFile = File.CreateText(outFileName);
                RAFObj = new iTextSharp.text.pdf.RandomAccessFileOrArray(inFileName);
                reader = new iTextSharp.text.pdf.PdfReader(RAFObj, null);


                Console.Write("Processing: ");

                var AccessionNos = new List<string>();
                string ExperimentNameFound = string.Empty;
                var keyFound = "Tube";
                var totalNoPage = reader.NumberOfPages;
                
                // search for the text...
                for (int page = 1; page <= reader.NumberOfPages; page++)
                {                   
                    pdftext = ExtractTextFromPDFBytes(reader.GetPageContent(page)) + " ";
                    pdftext = PdfTextExtractor.GetTextFromPage(reader, page) + " ";
                    

                    if (pdftext.Contains("\n\r") || pdftext.Contains("\n") || pdftext.Contains("\r"))
                    {
                        if (pdftext.Contains("\n\r"))
                        {
                            pdftext = pdftext.Replace("\n\r", " ").ToString();
                        }
                        //else if (pdftext.Contains("\n"))
                        //{
                        //    pdftext = pdftext.Replace("\n", " ").ToString();
                        //}
                        else if (pdftext.Contains("\r"))
                        {
                            pdftext = pdftext.Replace("\r", " ").ToString();
                        }
                    }

                    // if match found in the page..if text found then have it..otherwise search for image text if there any..
                    if (((pdftext.IndexOf(keyFound) != -1)))
                    {
                   
                         IndexpageCount++;
                        // searching accession no in pdfText..
                         var PdfTextArr = pdftext.Split('\n');
                         int TokenCount = 0;
                        foreach (var token in PdfTextArr)
                        {

                            bool RPT_Found = false;
                                 var NewToken = "";
                                    if (token.Contains("\n\r") || token.Contains("\n") || token.Contains("\r"))
                                    {
                                        if (token.Contains("\n\r"))
                                        {
                                            NewToken = token.Replace("\n\r", "").ToString();
                                        }
                                        else if (token.Contains("\n"))
                                        {
                                            NewToken = token.Replace("\n", "").ToString();
                                        }
                                        else if (token.Contains("\r"))
                                        {
                                            NewToken = token.Replace("\r", "").ToString();
                                        }
                                    }
                                    else
                                    {
                                        NewToken = token.ToString();
                                    }

                                    if (token.Contains("Experiment:"))
                                    { 
                                        string[] ExperimentSplit = token.Split(':');
                                        int CountOfExperimentSplits = ExperimentSplit.Count();
                                        ExperimentNameFound = ExperimentSplit[CountOfExperimentSplits-1];
                                    }

                                    if (NewToken.Length >= 8 || (NewToken.Contains("QC")) || NewToken.Contains("Cleaning Panel"))
                                    {
                                        if (NewToken == "E910839B06")
                                        { NewToken = "EP910839B06"; }
                                        if ((NewToken.Contains("_") || NewToken.Contains("-")) && (!(NewToken.Contains("QC")) && !NewToken.Contains("Cleaning Panel")))
                                        {
                                            if (NewToken.Contains("_"))
                                            {
                                                
                                                var SplitNewToken = NewToken.Split('_');
                                                NewToken = SplitNewToken[0];
                                                if (SplitNewToken[1] == "RPT")
                                                {
                                                    RPT_Found = true;
                                                }

                                            }
                                            else
                                            {
                                                var SplitNewToken = NewToken.Split('-');
                                                if (NewToken.Length == 10)
                                                {
                                                    NewToken = NewToken;
                                                }
                                                else
                                                {
                                                    NewToken = SplitNewToken[0];
                                                }
                                            }
                                        }
                                        if (NewToken.Contains(" ") && (!(NewToken.Contains("QC")) && !NewToken.Contains("Cleaning Panel")))
                                        {
                                            var SplitNewToken = NewToken.Split(' ');
                                            NewToken = SplitNewToken[0];
                                        }

                                        if (NewToken.Length >= 8 || (NewToken.Contains("QC")))
                                        {
                                            string CompareToken = NewToken.Substring(0, 2);
                                            string CompareToken_single = NewToken.Substring(0, 1);

                                            if (CompareToken == "AI" || (CompareToken == "AJ") || (CompareToken == "AP") || (CompareToken == "SP" && !NewToken.ToUpper().Contains("SPR")) || (CompareToken == "EF") || (CompareToken == "EE")
                                            || (CompareToken == "EP" && !NewToken.ToUpper().Contains("SEP")) || ((CompareToken == "S" || (CompareToken_single == "S" && NewToken.ToUpper() != "STATISTICS")) && !NewToken.Contains("SP") && !NewToken.ToUpper().Contains("SEP") && NewToken.Length == 10)
                                            || (CompareToken == "LP") || (CompareToken == "AK") || (CompareToken == "AL") || (CompareToken == "AM") || (CompareToken == "EG") || (CompareToken == "EH") || (CompareToken == "EI") || (CompareToken == "EJ")
                                            || (CompareToken == "SA") || (CompareToken == "SB") || (CompareToken == "SC") || (CompareToken == "YK") || (CompareToken == "QC") || (CompareToken == "YM") || (CompareToken == "PP") || (CompareToken == "LP")
                                            || ((CompareToken == "Cl") && (NewToken == "Cleaning Panel")) || (CompareToken_single == "Y"))
                                            {

                                                if ((NewToken.Length >= 8 && NewToken.Length <= 12) || (NewToken.Substring(0, 2) == "QC") ||  (NewToken == "Cleaning Panel"))
                                                {
                                                    var index = Array.FindIndex(PdfTextArr, r => r.Contains(token));
                                                    var nextFullValue = PdfTextArr[index + 3];
                                                    if(!nextFullValue.Contains("Skipped"))
                                                    {
                                                        if (RPT_Found)
                                                        { NewToken = NewToken + "_RPT"; }
                                                    AccessionNos.Add(NewToken.Trim());
                                                    }
                                                }

                                            }
                                        }
                                    }
                                    TokenCount++;

                        }    

                    }
                    else
                    {
                    break;
                   
                    }
                }

                // check if any acceesson no found
                if (AccessionNos.Count() > 0)
                {
                    AICount = AccessionNos.Count();
                    // here is the calculation
                    float dividend = (totalNoPage - (float)IndexpageCount) / AICount;// (50 -2) / 4 = 12 

                    bool isInt = dividend % 1 == 0;
                    if (isInt)
                    {
                        var pageBreakStartIndex = IndexpageCount + 1;
                        var start = pageBreakStartIndex;
                        var AssayName = string.Empty;
                        var ExperiemntName = string.Empty;
                        /////////  ***** OLD CODE ****** /////////////

                        //if (outPutPath.Contains(' '))
                        //{
                        //    var AssayNameSplit = outPutPath.Split(' ');
                        //    if (AssayNameSplit.Count() > 2)
                        //    {
                        //        if (AssayNameSplit[1] == "")
                        //        {
                        //            AssayName = AssayNameSplit[2];
                        //        }
                        //        else
                        //        {
                        //            AssayName = AssayNameSplit[1];
                        //        }
                        //    }
                        //}

                        if(ExperiemntName == "")
                        {
                            var ExperimentSplit = AssayOutputPath.Split('-');

                            ExperiemntName = ExperimentSplit[0];
                            if (ExperimentNameFound != string.Empty)
                            {
                                ExperiemntName = ExperimentNameFound.Trim();
                            }
                        
                        }

                        if (AssayName == "")
                        {


                            if (AssayOutputPath.Contains('_'))
                            {
                                var AssayNameSplit = AssayOutputPath.Split('_');
                                var FolderPath = AssayOutputPath.Split('\\');
                                int FolderNameCount = AssayNameSplit.Count();

                                string FolderName = AssayNameSplit[FolderNameCount-1];
                                AssayName = AssayNameSplit[1];
                                //if (FolderName != "")
                                //{
                                //    string RetriveAssay = FolderName.Substring(5);
                                //    AssayName = RetriveAssay.Substring(0, 4);
                                //}

                            }
                        } 

                        if (!Directory.Exists(outPutPath))
                            Directory.CreateDirectory(outPutPath);

                        foreach (var no in AccessionNos)
                        {
                            var outputFile = outPutPath + "\\" + no + "_" + AssayName + ".pdf";


                            // create document-object
                            Document document = new Document();
                            // create a writer that listens to the document
                            PdfCopy writer = new PdfCopy(document, new FileStream(outputFile, FileMode.Create));
                            document.Open();
                            for (start = pageBreakStartIndex; start <= (pageBreakStartIndex + (dividend - 1)); start++)
                            {
                                // split n merge here..
                                PdfImportedPage extractpage = writer.GetImportedPage(reader, start);
                                writer.AddPage(extractpage);
                            }
                            document.Close();
                            writer.Close();


                            #region Upload to LabMatrix

                            string details = CallRestMethod(no, ExperiemntName, outputFile);

                            #endregion

                            pageBreakStartIndex = start;
                        }
                    }
                    else
                    {
                        //Just find the fault cases//
                         AICount = 525 ;
                    }
                }

                else
                {
                    Console.WriteLine("Sorry, No Accession No found.");
                }

                return AICount;

            }
            catch (Exception ex)
             {
                return 0;
            }

            return AICount;

        }

        public static string CallRestMethod(string SpecimenName, string ExperimentName, string outputFileURL)
        {


            ServicePointManager.SecurityProtocol = (SecurityProtocolType)768 | (SecurityProtocolType)3072;
            var login_client = new RestClient("https://q2v.labmatrix.com/api/authentication/login");
            login_client.CookieContainer = new System.Net.CookieContainer();
            login_client.Timeout = -1;
            var login_request = new RestRequest(Method.POST);
            login_request.AddHeader("Authorization", "Basic YXBpX3VzZXI6ZGVtbzc3JiY=");
            login_request.AddHeader("contentType", "application/json");
            IRestResponse login_response = login_client.Execute(login_request);
            var cookie = ((login_response.Cookies[0]).Value);

            var client = new RestClient("https://q2v.labmatrix.com/api/qiagram/queryResults");
            client.CookieContainer = new System.Net.CookieContainer();
            client.Timeout = -1;
            var request = new RestRequest(Method.POST);
            request.AddHeader("Accept", "text/csv");
            request.AddHeader("Content-Type", "application/json");
            request.AddParameter("JSESSIONID", cookie, ParameterType.Cookie);
            request.AddParameter("application/json", "{\"apiKey\": \"q2.flow.attachments\", \"variables\": {\"Specimen Name\": \"" + SpecimenName + "\",\"Experiment Name\": \"" + ExperimentName + "\"}}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            string URLResponse = response.Content.ToString().Replace("ID", "").Replace("\n", "").Replace("\r", "");
            Console.WriteLine("First Response." + URLResponse);

            var client_Attach = new RestClient("https://q2v.labmatrix.com/api/Biomaterial/" + URLResponse + "/attachments/");

            client_Attach.Timeout = -1;
            var request_attach = new RestRequest(Method.POST);
            request_attach.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request_attach.AddHeader("Accept", "*/*");
            request_attach.AddParameter("JSESSIONID", cookie, ParameterType.Cookie);
            request_attach.AddFile("file", outputFileURL);
            IRestResponse response_attach = client_Attach.Execute(request_attach);
            Console.WriteLine("2nd Response." + response_attach.Content.ToString());

            var logout_client = new RestClient("https://q2v.labmatrix.com/api/authentication/logout");
            var logout_request = new RestRequest(Method.GET);
            logout_request.AddParameter("JSESSIONID", cookie, ParameterType.Cookie);
            logout_client.Execute(logout_request);
            string UploadedLog = string.Empty;
            if (response_attach.Content.ToString().Contains("error"))
            {
                UploadedLog = "Successfully Uploaded.";
            }
            else
            { UploadedLog = "Upload failed."; }
            return UploadedLog;

          
        }

        public int SearchAndMergePDF(string inFileName, string outputfile, string key)
        {

            // Code for 

            string outputPath = @"";

            var dataPath = @"";
            string tempPath = "";

            string pdftext = string.Empty;
            string pdfImagetext = string.Empty;
            int pageCount = 0;
            try
            {
                // create document-object
                Document document = new Document();

                // create a writer that listens to the document
                PdfCopy writer = new PdfCopy(document, new FileStream(outputfile, FileMode.Create));
                if (writer == null)
                {
                    return 0;
                }

                // open the document
                document.Open();
                iTextSharp.text.pdf.RandomAccessFileOrArray RAFObj = null;
                iTextSharp.text.pdf.PdfReader reader = null;
                iTextSharp.text.pdf.PdfObject obj = null;
                // Create a reader for the given PDF file
                // PdfReader reader = new PdfReader(inFileName);
                //outFile = File.CreateText(outFileName);
                RAFObj = new iTextSharp.text.pdf.RandomAccessFileOrArray(inFileName);
                reader = new iTextSharp.text.pdf.PdfReader(RAFObj, null);

                Console.Write("Processing: ");

                var index = 0;

                if (!Directory.Exists(outputPath))
                    Directory.CreateDirectory(outputPath);

                // search for the text...
                for (int page = 1; page <= reader.NumberOfPages; page++)
                {
                    pdftext = ExtractTextFromPDFBytes(reader.GetPageContent(page)) + " ";

                    // if match found in the page..if text found then have it..otherwise search for image text if there any..
                    if (pdftext.IndexOf(key) != -1)
                    {
                        pageCount++;
                        PdfImportedPage extractpage = writer.GetImportedPage(reader, page);
                        writer.AddPage(extractpage);
                    }
                    else
                    {
                        // searching for images...
                        //PdfDictionary pg = reader.GetPageN(page);
                        obj = reader.GetPdfObject(page - 1);// FindImageInPDFDictionary(pg);
                        if ((obj != null) && obj.IsStream())
                        {

                            // int XrefIndex = Convert.ToInt32(((PRIndirectReference)obj).Number.ToString(System.Globalization.CultureInfo.InvariantCulture));
                            // PdfObject pdfObj = reader.GetPdfObject(XrefIndex);
                            PdfStream pdfStrem = (PdfStream)obj;
                            PdfObject subtype = pdfStrem.Get(PdfName.SUBTYPE);

                            if ((subtype != null) && subtype.ToString() == iTextSharp.text.pdf.PdfName.IMAGE.ToString())
                            {
                                try
                                {
                                    iTextSharp.text.pdf.parser.PdfImageObject PdfImageObj =
                             new iTextSharp.text.pdf.parser.PdfImageObject((iTextSharp.text.pdf.PRStream)pdfStrem);

                                    System.Drawing.Image ImgPDF = PdfImageObj.GetDrawingImage();
                                    using (var newBitmap = new Bitmap(ImgPDF))
                                    {
                                        tempPath = System.IO.Path.Combine(outputPath, String.Format(@"{0}_{1}.jpg", page, index++));// 1_1.jpg
                                        newBitmap.Save(tempPath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                    }
                                }
                                catch (Exception f)
                                { }


                                // here we use OCR to read image text by testing trained data
                                try
                                {
                                    pdfImagetext = string.Empty;
                                    using (var tEngine = new TesseractEngine(dataPath, "eng", EngineMode.TesseractAndCube)) //creating the tesseract OCR engine with English as the language
                                    {
                                        using (var img = Pix.LoadFromFile(tempPath)) // Load of the image file from the Pix object which is a wrapper for Leptonica PIX structure
                                        {

                                            using (var ipage = tEngine.Process(img)) //process the specified image
                                            {
                                                pdfImagetext = ipage.GetText(); //Gets the image's content as plain text.
                                                                                // Console.WriteLine(page.GetMeanConfidence()); //Get's the mean confidence that as a percentage of the recognized text.
                                                                                /// see any match found..

                                                //  if (pdfImagetext.IndexOf(key) != -1)
                                                if (maxMatchOccuranceInImage(pdfImagetext, key, 0))
                                                {
                                                    pageCount++;
                                                    PdfImportedPage extractpage = writer.GetImportedPage(reader, page);
                                                    writer.AddPage(extractpage);
                                                }
                                            }
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    //Console.WriteLine("Unexpected Error: " + e.Message);
                                }
                            }
                        }
                    }
                }

                reader.Close();
                // close the document and writer
                writer.Close();
                document.Close();

                return pageCount;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
                return pageCount;
            }
            finally
            {
                // if (outFile != null) outFile.Dispose();
            }
        }

        public System.Drawing.Image byteArrayToImage(byte[] byteArrayIn)
        {

            System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
            System.Drawing.Image img = (System.Drawing.Image)converter.ConvertFrom(byteArrayIn);

            return img;
        }

        // recursion to match the max. occurance of charecters in key
        // min. charecters should be at least 3..
        public bool maxMatchOccuranceInImage(string imgText, string inputkey, int start)
        {
            var inputSubKey = inputkey.Substring(start, inputkey.Length - (start));
            if (inputSubKey == "" || inputSubKey.Length <= 0)
            {
                return false;
            }
            if (inputkey.Length > 3 && inputSubKey.Length <= 3)
            {
                return false;
            }


            if (imgText.IndexOf(inputSubKey) != -1)
            {
                return true;
            }
            else
            {
                return maxMatchOccuranceInImage(imgText, inputkey, ++start);// abcde  5-1+1=3
            }
        }

        public static System.Drawing.Imaging.ImageCodecInfo GetImageEncoder(string imageType)
        {
            imageType = imageType.ToUpperInvariant();



            foreach (ImageCodecInfo info in ImageCodecInfo.GetImageEncoders())
            {
                if (info.FormatDescription == imageType)
                {
                    return info;
                }
            }

            return null;
        }

        private static PdfObject FindImageInPDFDictionary(PdfDictionary pg)
        {
            PdfDictionary res =
                (PdfDictionary)PdfReader.GetPdfObject(pg.Get(PdfName.RESOURCES));


            PdfDictionary xobj =
              (PdfDictionary)PdfReader.GetPdfObject(res.Get(PdfName.XOBJECT));
            if (xobj != null)
            {
                foreach (PdfName name in xobj.Keys)
                {

                    PdfObject obj = xobj.Get(name);
                    if (obj.IsIndirect())
                    {
                        PdfDictionary tg = (PdfDictionary)PdfReader.GetPdfObject(obj);

                        PdfName type =
                          (PdfName)PdfReader.GetPdfObject(tg.Get(PdfName.SUBTYPE));

                        //image at the root of the pdf
                        if (PdfName.IMAGE.Equals(type))
                        {
                            return obj;
                        }// image inside a form
                        else if (PdfName.FORM.Equals(type))
                        {
                            return FindImageInPDFDictionary(tg);
                        } //image inside a group
                        else if (PdfName.GROUP.Equals(type))
                        {
                            return FindImageInPDFDictionary(tg);
                        }

                    }
                }
            }

            return null;

        }

        /*
        //Write the progress.
           /* int totalLen = 68;
                 float charUnit = ((float)totalLen) / (float)reader.NumberOfPages;
                 int totalWritten = 0;
                 float curUnit = 0; 
                            if (charUnit >= 1.0f)
                            {
                                for (int i = 0; i< (int)charUnit; i++)
                                {
                                    Console.Write("#");
                                    totalWritten++;
                                }
        }
                            else
                            {
                                curUnit += charUnit;
                                if (curUnit >= 1.0f)
                                {
                                    for (int i = 0; i< (int)curUnit; i++)
                                    {
                                        Console.Write("#");
                                        totalWritten++;
                                    }
                                    curUnit = 0;
                                }

                            } 
                            if (totalWritten < totalLen)
                        {
                            for (int i = 0; i < (totalLen - totalWritten); i++)
                            {
                                Console.Write("#");
                            }
                        }
         */

        #region ExtractTextFromPDFBytes
        /// <summary>
        /// This method processes an uncompressed Adobe (text) object 
        /// and extracts text.
        /// </summary>
        /// <param name="input">uncompressed</param>
        /// <returns></returns>
        private string ExtractTextFromPDFBytes(byte[] input)
        {
            if (input == null || input.Length == 0) return "";

            try
            {
                string resultString = "";

                // Flag showing if we are we currently inside a text object
                bool inTextObject = false;

                // Flag showing if the next character is literal 
                // e.g. '\\' to get a '\' character or '\(' to get '('
                bool nextLiteral = false;

                // () Bracket nesting level. Text appears inside ()
                int bracketDepth = 0;

                // Keep previous chars to get extract numbers etc.:
                char[] previousCharacters = new char[_numberOfCharsToKeep];
                for (int j = 0; j < _numberOfCharsToKeep; j++) previousCharacters[j] = ' ';


                for (int i = 0; i < input.Length; i++)
                {
                    char c = (char)input[i];

                    if (inTextObject)
                    {
                        // Position the text
                        if (bracketDepth == 0)
                        {
                            if (CheckToken(new string[] { "TD", "Td" }, previousCharacters))
                            {
                                resultString += "\n\r";
                            }
                            else
                            {
                                if (CheckToken(new string[] { "'", "T*", "\"" }, previousCharacters))
                                {
                                    resultString += "\n";
                                }
                                else
                                {
                                    if (CheckToken(new string[] { "Tj" }, previousCharacters))
                                    {
                                        resultString += " ";
                                    }
                                }
                            }
                        }

                        // End of a text object, also go to a new line.
                        if (bracketDepth == 0 &&
                            CheckToken(new string[] { "ET" }, previousCharacters))
                        {

                            inTextObject = false;
                            resultString += " ";
                        }
                        else
                        {
                            // Start outputting text
                            if ((c == '(') && (bracketDepth == 0) && (!nextLiteral))
                            {
                                bracketDepth = 1;
                            }
                            else
                            {
                                // Stop outputting text
                                if ((c == ')') && (bracketDepth == 1) && (!nextLiteral))
                                {
                                    bracketDepth = 0;
                                }
                                else
                                {
                                    // Just a normal text character:
                                    if (bracketDepth == 1)
                                    {
                                        // Only print out next character no matter what. 
                                        // Do not interpret.
                                        if (c == '\\' && !nextLiteral)
                                        {
                                            nextLiteral = true;
                                        }
                                        else
                                        {
                                            if (((c >= ' ') && (c <= '~')) ||
                                                ((c >= 128) && (c < 255)))
                                            {
                                                resultString += c.ToString();
                                            }

                                            nextLiteral = false;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // Store the recent characters for 
                    // when we have to go back for a checking
                    for (int j = 0; j < _numberOfCharsToKeep - 1; j++)
                    {
                        previousCharacters[j] = previousCharacters[j + 1];
                    }
                    previousCharacters[_numberOfCharsToKeep - 1] = c;

                    // Start of a text object
                    if (!inTextObject && CheckToken(new string[] { "BT" }, previousCharacters))
                    {
                        inTextObject = true;
                    }
                }
                return resultString;
            }
            catch (Exception ex)
            {
                return "error " + ex.Message;
            }
        }
        #endregion

        #region CheckToken
        /// <summary>
        /// Check if a certain 2 character token just came along (e.g. BT)
        /// </summary>
        /// <param name="search">the searched token</param>
        /// <param name="recent">the recent character array</param>
        /// <returns></returns>
        private bool CheckToken(string[] tokens, char[] recent)
        {
            foreach (string token in tokens)
            {
                try
                {
                    char nextToken = (token.Length >= 2) ? token[1] : ' ';
                    if ((recent[_numberOfCharsToKeep - 3] == token[0]) &&
                        (recent[_numberOfCharsToKeep - 2] == nextToken) &&
                        ((recent[_numberOfCharsToKeep - 1] == ' ') ||
                        (recent[_numberOfCharsToKeep - 1] == 0x0d) ||
                        (recent[_numberOfCharsToKeep - 1] == 0x0a)) &&
                        ((recent[_numberOfCharsToKeep - 4] == ' ') ||
                        (recent[_numberOfCharsToKeep - 4] == 0x0d) ||
                        (recent[_numberOfCharsToKeep - 4] == 0x0a))
                        )
                    {
                        return true;
                    }
                    count++;
                }
                catch (Exception e)
                {

                    return false;
                }
            }
            return false;
        }
        #endregion
    }
}
