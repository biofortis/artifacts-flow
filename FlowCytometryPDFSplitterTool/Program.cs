﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Configuration;
using System.Text;
using PdfToText;


namespace FlowCytometryPDFSplitterTool
{
    class Program
    {
        static void Main(string[] args)
        {
            string ExtAllowed = ".pdf";

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("\n\nProgram Developed by : Praveen Yedunuthala");
            Console.ResetColor();

        //Initialising the system paths
            string outputPDFPath = "C:\\LabMatrix\\PDF_SplitterService_Output\\";
            string selectedAccessionPDFPath = "C:\\LabMatrix\\PDF_SplitterService_Output\\";
            string LogFiles = "C:\\LabMatrix\\PDF_SplitterService_Logs\\";
            //string outputPDFPath = "\\\\dubsqqpps01p\\PDF_SplitterService_Output\\";
            //string selectedAccessionPDFPath = "\\\\dubsqqpps01p\\PDF_SplitterService_Output\\";
            //string LogFiles = "\\\\dubsqqpps01p\\PDF_SplitterService_Logs";



            //string outputPDFPath = "C:\\Users\\Q827534\\Desktop\\FlowCytometry_Tool\\FlowCytometry_OutPut\\";
            //string selectedAccessionPDFPath = "C:\\Users\\Q827534\\Desktop\\FlowCytometry_Tool\\FlowCytometry_OutPut\\";
            //string LogFiles = "C:\\Users\\Q827534\\Desktop\\FlowCytometry_Tool\\FlowCytometry_OutPut\\";
            Program This = new Program();
            This.PDFSplitter(outputPDFPath, selectedAccessionPDFPath, ExtAllowed, LogFiles);
            //Console.Read();

        }

        


        public void PDFSplitter(string outputPDFPath, string selectedAccessionPDFPath, string ExtAllowed, string LogFiles)
        {

            PDFParser pdfParser = new PDFParser();
            int AccessionNoCount = 0;
            string[] listOfFolders = new string[9];// = new string[3];
           int TOTALNOOFPPATH;


            string CurrentDateTime = DateTime.Now.ToString("MM-dd-yyyy hh-mm-ss ");
            string PrintCurrentDateTime = DateTime.Now.ToString("MM-dd-yyyy h:mm tt");



            //#region Deleting the old files 


            //var Logdirectory = new DirectoryInfo(LogFiles);
            //DateTime LogFile_from_date = DateTime.Now.AddMonths(-1);
            //DateTime LogFile_to_date = DateTime.Now;
            //var LogFiles_FileTocheck = Logdirectory.GetFileSystemInfos("*.rtf", SearchOption.AllDirectories).Where(File => File.CreationTime > LogFile_from_date);

            //        foreach (var LogFile_file in LogFiles_FileTocheck)
            //        {
            //            File.Delete((LogFile_file.ToString()));
            //        }


            //#endregion

            string fileName = LogFiles + "AutoEmailerLog_" + CurrentDateTime + ".rtf";

            using (StreamWriter dtxLog = new StreamWriter(fileName, false))
            {


                StringBuilder sb123 = new StringBuilder();
                sb123.Append("<table style='width:100%';>");
                dtxLog.Write(sb123.ToString());
                StringBuilder sb0123 = new StringBuilder();
                sb0123.Append("<tr><td colspan='4' align='center'>").Append("<h3>PDF Auto Splitter service Log</h3>").Append("</td></tr>");
                dtxLog.Write(sb0123.ToString());
                StringBuilder sb01231 = new StringBuilder();
                sb01231.Append("<tr><td colspan='4' align='center'>").Append(System.Environment.NewLine).Append("</td></tr>");
                dtxLog.Write(sb01231.ToString());               


                StringBuilder sb1231 = new StringBuilder();
                sb1231.Append("</table>");
                dtxLog.Write(sb1231.ToString());


                StringBuilder sb12342 = new StringBuilder();
                sb12342.Append("<table style='width:100%';>");
                dtxLog.Write(sb12342.ToString());

                StringBuilder ls0 = new StringBuilder();
                ls0.Append("<tr><td  colspan='4'>").Append("<b>Processed Files:</b>").Append("</td></tr>");
                dtxLog.Write(ls0.ToString());


                StringBuilder ls1 = new StringBuilder();
                ls1.Append("<tr><td width='30%' style='border:1px Gray Solid;'>").Append("<b>File Name</b>").Append("</td>");
                ls1.Append("<td width ='50%' style='border:1px Gray Solid;'>").Append("<b>File Path</b>").Append("</td>");
                ls1.Append("<td width='5%' style='border:1px Gray Solid;'>").Append("<b>Accessions Found</b>").Append("</td>");
                ls1.Append("<td width = '15%' style='border:1px Gray Solid;'>").Append("<b>Split Status</b>").Append("</td></tr>");
                dtxLog.Write(ls1.ToString());
                
                if (!Directory.Exists(outputPDFPath))
                {
                    Directory.CreateDirectory(outputPDFPath);
                }
                if (!Directory.Exists(selectedAccessionPDFPath))
                {
                    Directory.CreateDirectory(selectedAccessionPDFPath);
                }

                // loop th. folders

                // Here Making the Sevice based Call.///
              //listOfFolders[0] = "C:\\Users\\Q827534\\Desktop\\Installtions Temp Folder\\ToolInputs\\NotWorking";
                //listOfFolders[0] = "F:\\FlowCytometryPDFSplitterServiceTool\\TestInputFiles";
                //DEV
               // listOfFolders[0] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\DEV";
                //UAT
                //listOfFolders[0] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\TSL\\LabMatrix";


                //PROD
                listOfFolders[0] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\LabmatrixPDF";

                //listOfFolders[0] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\QLA\\2020\\";
                //listOfFolders[1] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\QLC\\2020\\";
                //listOfFolders[2] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\QLE\\2020\\";
                //listOfFolders[3] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\QLI\\2020\\";
                //listOfFolders[4] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\QLJ\\2020\\";
                //listOfFolders[5] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\QLP\\2020\\";
                //listOfFolders[6] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\QLS\\2020\\";
                //listOfFolders[7] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\QLV\\2020\\";
                //listOfFolders[8] = "\\\\quintiles.net\\Enterprise\\Apps\\FACSCanto\\Analysis\\QTGD\\2020\\";

                

                
                for (int folderIndex = 0; folderIndex < listOfFolders.Length; folderIndex++)
                {
                    if (listOfFolders[folderIndex] != null && listOfFolders[folderIndex].Trim() != string.Empty && Directory.Exists(listOfFolders[folderIndex].Trim()))
                    {
                        // search pdf files from root and its subfolders Last 3 months

                        var directory = new DirectoryInfo(listOfFolders[folderIndex]);
                        DateTime from_date = DateTime.Now.AddMonths(-3);
                        DateTime to_date = DateTime.Now;
                        var FileTocheck = directory.GetFileSystemInfos("*" + ExtAllowed, SearchOption.AllDirectories).Where(File => File.CreationTime >= from_date && File.CreationTime <= to_date);
                        //Array Files1 = FileTocheck.ToArray();

                        //string[] pdfFiles = Directory.GetFiles(listOfFolders[folderIndex], "*" + ExtAllowed, SearchOption.AllDirectories);



                        Console.WriteLine(" \nTotal :" + FileTocheck.Count() + " files found for processing.");

                        

                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("\n Please do not close or interrupt in the middle, instead you can minimise.");
                        Console.ResetColor();

                        if (FileTocheck.Count() > 0)
                        {
                            bool QualifiedFiles = false;
                            // loop th. pdf files found..
                            StringBuilder ls_123 = new StringBuilder();
                            foreach (var loopFile in FileTocheck)
                            {
                                // set output path
                                string pdf = (loopFile.FullName.ToString());
                                var AssayoutputPath = Path.GetFileNameWithoutExtension(pdf);
                                var outputPath = outputPDFPath;

                                DateTime LastCreationTime = File.GetCreationTime(pdf);

                                if (LastCreationTime > Convert.ToDateTime("1/1/2000"))
                                {
                                    QualifiedFiles = true;
                                    //DateTime NewDate = new DateTime();
                                    File.SetCreationTime(pdf, new DateTime(2000, 01, 01));
                                    var LastAccessTime = File.GetLastWriteTime(pdf);
                                    //var LastAccessTime = File.GetAttributes(outputPath);
                                    //if (LastAccessTime >= DateTime.Now)
                                    //{ 

                                    //}

                                   
                                    ls_123.Append("<tr><td style='border:1px Gray Solid;'>").Append(Path.GetFileName(pdf)).Append("</td>");
                                    ls_123.Append("<td style='border:1px Gray Solid;'>").Append(Path.GetDirectoryName(pdf)).Append("</td>");
                                    // get the accession count
                                    AccessionNoCount = pdfParser.SplitAndMergePDF(Path.GetFullPath(pdf), outputPath, AssayoutputPath);

                                    if (AccessionNoCount == 525)
                                    {
                                        ls_123.Append("<td style='border:1px Gray Solid;'>").Append("Mismatch").Append("</td>");
                                        ls_123.Append("<td style='border:1px Gray Solid;'>").Append("Failed").Append("</td></tr>");
                                        dtxLog.Write(ls_123.ToString());
                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Mismatch in Accessions list and images in the pdf, PDF Excluded. " + Path.GetFileName(pdf) + "\n");
                                        Console.ResetColor();

                                    }
                                    else if (AccessionNoCount > 0)
                                    {

                                        ls_123.Append("<td style='border:1px Gray Solid;'>").Append(AccessionNoCount).Append("</td>");
                                        ls_123.Append("<td style='border:1px Gray Solid;'>").Append("Success.").Append("</td></tr>");
                                        dtxLog.Write(ls_123.ToString());


                                        Console.ForegroundColor = ConsoleColor.Green;
                                        Console.WriteLine("Total " + AccessionNoCount + " Accessions found and all splitted and merged. " + Path.GetFileName(pdf) + "\n");
                                        Console.ResetColor();

                                    }
                                    else
                                    {
                                        ls_123.Append("<td style='border:1px Gray Solid;'>").Append("None").Append("</td>");
                                        ls_123.Append("<td style='border:1px Gray Solid;'>").Append("Failed").Append("</td></tr>");
                                        dtxLog.Write(ls_123.ToString());

                                        Console.ForegroundColor = ConsoleColor.Red;
                                        Console.WriteLine("Sorry, No Accession no. found in " + Path.GetFileName(pdf) + "\n");
                                        Console.ResetColor();

                                    }

                                    
                                }                               

                            }
                            if (!QualifiedFiles)
                            {
                                
                                ls_123.Append("<tr><td colspan='4' style='border:1px Gray Solid;'>").Append("No files qulified to process.").Append("</td></tr>");
                                dtxLog.Write(ls_123.ToString());
                            }


                        }
                        else
                        {
                            StringBuilder ls = new StringBuilder();
                            ls.Append("<tr><td colspan='4' style='border:1px Gray Solid;'>").Append(": No files qulified to process.").Append("</td></tr>");
                            dtxLog.Write(ls.ToString());
                        }

                        //Here We Delete
                        directory.GetFiles("*" + ExtAllowed, SearchOption.AllDirectories).Where(File => File.CreationTime == Convert.ToDateTime("1/1/2000")).ToList().ForEach(File => File.Delete());

                        


                    }
                }
                StringBuilder sb123452 = new StringBuilder();
                sb123452.Append("</table>");
                dtxLog.Write(sb123452.ToString());
            }
            
        }
    }
}
